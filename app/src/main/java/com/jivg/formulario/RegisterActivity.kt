package com.jivg.formulario

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.CompoundButton
import com.jivg.formulario.Model.Movie
import kotlinx.android.synthetic.main.activity_detail_movie.*


class RegisterActivity : AppCompatActivity() {

    val hashMap:HashMap<String,String> = HashMap<String,String>()
    var condiciones = false
    lateinit var movie: Movie


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        /*
        textInputLayoutNombre.isErrorEnabled = true
        textInputLayoutApellidos.isErrorEnabled = true
        textInputLayoutDireccion.isErrorEnabled = true
        */

        movie = intent.extras?.getSerializable("movie") as Movie
        if (movie.id % 3 == 1)
            imageViewPelicula.setImageResource(R.drawable.cementerio)
        else if ( movie.id % 3 == 2)
            imageViewPelicula.setImageResource(R.drawable.purga)
        else
            imageViewPelicula.setImageResource(R.drawable.monja)



        imageButtonTrailer.setImageResource(R.drawable.ic_movie)

        textInputLayoutNombre.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val nombre=textInputLayoutNombre.editText?.text.toString()

            if (!hasFocus){
                if (nombre.isNotEmpty()) {
                    textInputLayoutNombre.isErrorEnabled = false
                }else {
                    textInputLayoutNombre.isErrorEnabled = true
                    textInputLayoutNombre.error = "El nombre es incorrecto"
                }
            }
        })

        textInputLayoutApellidos.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val apellidos=textInputLayoutApellidos.editText?.text.toString()

            if (!hasFocus){
                if (apellidos.isNotEmpty()) {
                    textInputLayoutApellidos.isErrorEnabled = false
                }else {
                    textInputLayoutApellidos.isErrorEnabled = true
                    textInputLayoutApellidos.error = "Los apellidos son incorrectos"
                }
            }
        })

        textInputLayoutDireccion.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val direccion=textInputLayoutDireccion.editText?.text.toString()

            if (!hasFocus){
                if (direccion.isNotEmpty()) {
                    textInputLayoutDireccion.isErrorEnabled = false
                }else {
                    textInputLayoutDireccion.isErrorEnabled = true
                    textInputLayoutDireccion.error = "La direccion es incorrecta"
                }
            }
        })


        var idiomaSeleccionado = "Español"
        radioGroupIdioma.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                radioButtonEspaniol.id ->
                    idiomaSeleccionado = "Español"
                radioButtonSubtitulada.id ->
                    idiomaSeleccionado = "Subtitulado"
            }
        }

        val tiposCalidad = arrayListOf<String>("SD", "HD", "3D", "4K")
        val calidadAdapter = ArrayAdapter<String>(baseContext,android.R.layout.simple_spinner_item, tiposCalidad)
        calidadAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // Ocupa una vista por default
        spinnerCalidad.adapter=calidadAdapter

        spinnerCalidad.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val calidadSeleccionada = tiposCalidad[position]
                hashMap.put("calidad", calidadSeleccionada)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })

        //var condiciones = checkBoxTerminos.isChecked

        checkBoxTerminos.setOnCheckedChangeListener(onCheckedChangeListener)

        imageButtonTrailer.setOnClickListener(View.OnClickListener {
            val id = "t-eSNuv28XQ"
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id")
            )
            try {
                startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                startActivity(webIntent)
            }

        })

        fab.setOnClickListener { view ->
            if(condiciones){
                if(textInputLayoutNombre.editText?.text.toString() != "" && textInputLayoutApellidos.editText?.text.toString() != "" && textInputLayoutDireccion.editText?.text.toString() != ""){
                    Toast.makeText(baseContext,"Se guardo la informacion",Toast.LENGTH_LONG).show()
                    if (!textInputLayoutNombre.isErrorEnabled)
                        hashMap.put("nombre", textInputLayoutNombre.editText?.text.toString())
                    if (!textInputLayoutApellidos.isErrorEnabled)
                        hashMap.put("apellidos", textInputLayoutApellidos.editText?.text.toString())
                    if (!textInputLayoutDireccion.isErrorEnabled)
                        hashMap.put("direccion", textInputLayoutDireccion.editText?.text.toString())
                    hashMap.put("idioma",idiomaSeleccionado)
                    hashMap.put("politicas","Aceptados")

                    val detailIntent = Intent(this, DetailRegisterActivity::class.java)
                    detailIntent.putExtra("datos", hashMap)
                    startActivity(detailIntent)
                }else{
                    Toast.makeText(baseContext,"Debe llenar todos los campos",Toast.LENGTH_LONG).show()
                }
            }else{
                Toast.makeText(baseContext,"Debe aceptar los terminos y condiciones",Toast.LENGTH_LONG).show()
            }




        }
    }


    private val onCheckedChangeListener: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                condiciones = isChecked }



}
