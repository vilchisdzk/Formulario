package com.jivg.formulario

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailRegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val hashMap:HashMap<String,String>  = intent.extras?.getSerializable("datos") as HashMap<String,String>

        val nombre = hashMap.get("nombre")
        val apellidos = hashMap.get("apellidos")
        val direccion = hashMap.get("direccion")
        val idioma = hashMap.get("idioma")
        val calidad = hashMap.get("calidad")
        val politicas = hashMap.get("politicas")


        detailTextViewNombreCompleto.setText("Nombre: ${nombre} ${apellidos}")
        detailTextViewDireccion.setText("Direccion: ${direccion}")
        detailTextViewCalidad.setText("Calidad: ${calidad}")
        detailTextViewIdioma.setText("Idioma: ${idioma}")
        detailTextViewTerminos.setText("Terminos y Condiciones: ${politicas}")


        buttonAceptar.setOnClickListener(View.OnClickListener {

            val mainIntent = Intent(baseContext, ListaMoviesActivity::class.java)
            startActivity(mainIntent)
        })
    }
}
