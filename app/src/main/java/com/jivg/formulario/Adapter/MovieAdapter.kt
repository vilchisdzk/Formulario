package com.jivg.formulario.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jivg.formulario.DetailMovieActivity
import com.jivg.formulario.Model.Movie
import com.jivg.formulario.R
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    var movies: MutableList<Movie> = ArrayList()
    lateinit var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_movie,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return movies.size
    }


    fun RecyclerAdapter(movies: MutableList<Movie>, context: Context) {
        this.movies = movies
        this.context = context
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies.get(position)
        holder.bind(item, context)
    }

    public fun addMovie(movie: Movie) {
        movies.add(movie)
        notifyDataSetChanged()

    }

    public fun editMovie(movie: Movie, position: Int) {
        movies.add(position, movie)
        notifyItemInserted(position)
    }

    public fun removeContact(position: Int) {
        if (movies.size != 0) {
            movies.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    public fun removeLastMoviet() {
        if (movies.size != 0) {
            val position: Int = movies.size - 1
            movies.removeAt(position)
            notifyItemRemoved(position)
        }
    }


    class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val movieImageView = view.imageViewPeli
        val tituloTextView = view.textViewTitPelicula
        val anioTextView = view.textViewAnioPelicula
        val descripcionTextView = view.textViewDescripcionPelicula


        private lateinit var movie: Movie

        fun bind(movie: Movie, context: Context) {

            tituloTextView?.text = movie.titulo
            anioTextView?.text = movie.anio
            descripcionTextView?.text = movie.descripcion

            if (movie.id%3 == 1)
                movieImageView.setImageResource(R.drawable.cementerio)
            else if ( movie.id%3 == 2)
                movieImageView.setImageResource(R.drawable.purga)
            else
                movieImageView.setImageResource(R.drawable.monja)


            itemView.setOnClickListener(View.OnClickListener {
                val mainIntent = Intent(context, DetailMovieActivity::class.java)
                mainIntent.putExtra("movie",movie)
                itemView.context.startActivity(mainIntent)
            })
        }


    }
}