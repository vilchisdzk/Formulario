package com.jivg.formulario

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.jivg.formulario.Adapter.MovieAdapter
import com.jivg.formulario.Model.Movie
import kotlinx.android.synthetic.main.activity_list_movies.*




class ListaMoviesActivity : AppCompatActivity(){

    val mAdapter : MovieAdapter = MovieAdapter()


    //private lateinit var contactsRecyclerView:RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_movies)
//        setSupportActionBar(toolbar)

        moviesRecyclerView.setHasFixedSize(true)
        moviesRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.RecyclerAdapter(getMovies(), this)
        moviesRecyclerView.adapter = mAdapter
    }

    fun getMovies(): MutableList<Movie> {
        var movies:MutableList<Movie> = ArrayList()
        movies.add(Movie(1,"Cementerio de Mascotas", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(2,"La Purga", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(3,"La Monja", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))
        movies.add(Movie(4,"Cementerio de Mascotas 2", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(5,"La Purga 2", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(6,"La Monja 2", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))

        return movies
    }






}
