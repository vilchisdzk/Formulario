package com.jivg.formulario

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.jivg.formulario.Model.Movie
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    lateinit var movie:Movie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);

        movie = intent.extras?.getSerializable("movie") as Movie

        val id = movie.id
        val titulo = movie.titulo
        val descripcion = movie.descripcion
        val anio = movie.anio


        textViewDetTitulo.text = titulo
        textViewDetAnio.text = anio
        textViewDetDesc.text = descripcion


        if (id % 3 == 1)
            imageView.setImageResource(R.drawable.cementerio)
        else if ( id % 3 == 2)
            imageView.setImageResource(R.drawable.purga)
        else
            imageView.setImageResource(R.drawable.monja)


        buttonDetReservar.setOnClickListener(View.OnClickListener {

            val mainIntent = Intent(baseContext, RegisterActivity::class.java)
            mainIntent.putExtra("movie",movie)
            startActivity(mainIntent)
        })

    }
}
